import React, {Component} from 'react';
import {Container} from 'react-bootstrap';

import Bar from './Bar.js';
import PadelCard from './PadelCard.js';
import BuyTokenCard from './BuyTokenCard.js';

import styles from './App.module.scss';



class App extends Component {
	constructor(props) {

		
		super(props);
		this.state = {
			accounts:[]
		};
	
	}

	async componentDidMount(){
		const accounts = await  window.ethereum.request({ method: 'eth_requestAccounts' });
		console.log("accounts",accounts);
		this.setState({accounts})
	}

	render() {
		return (
      <div>
        <Bar/>
			  <Container expand="lg" className={styles.App}>
          <PadelCard/>
          <BuyTokenCard/>
        </Container>

      </div>
		);
	}
}


export default App;