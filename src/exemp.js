import React from 'react';

import { useWeb3Injected, useWeb3Network } from '@openzeppelin/network/react';
import Web3Info from './components/Web3Info/index.js';
import Web3 from 'web3'
import styles from './App.module.scss';
import contractJson from '/Users/fabri/Documents/personali/dapps/PADDY/build/contracts/Paddy.json';

const infuraToken = '69ab2c89571f422f842756d05067ee59';

function App() {
  const injected = useWeb3Injected();
  const isHttp = window.location.protocol === 'http:';
  const local = useWeb3Network('http://127.0.0.1:8545');
  const network = useWeb3Network(`https://ropsten.infura.io/v3/69ab2c89571f422f842756d05067ee59`, {
    pollInterval: 10 * 1000,
  })
  const contract = new Web3.eth.Contract(contractJson, "0x8Da1649554EC5f59374C9d2FBBd4A91c157eb759");
  console.log(contract.methods.name());

  return (
    <>
      <h1>OpenZeppelin Starter Kit</h1>
      <div className={styles.App}>
        {injected && <Web3Info title="Wallet Web3" web3Context={injected} />}
        {isHttp && <Web3Info title="Local Web3 Node" web3Context={local} />}
        {infuraToken && <Web3Info title="Infura Web3" web3Context={network} />}
      </div>
    </>
  );
}

export default App;
