import React, { Component } from 'react';
import { Card, Button } from 'react-bootstrap';
import Web3 from 'web3';
import PaddyJson from '../contracts/Paddy.json';


import styles from './App.module.scss';
const PADDY_ADDRESS = '0xAb5e9c857b5747D38138CC4Ac2788aA87Bd5FfD4';

class BuyTokenCard extends Component {
	constructor(props) {
		super(props);
		this.web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/69ab2c89571f422f842756d05067ee59"));
		//console.log(Web3.givenProvider);
		this.contract = new this.web3.eth.Contract(PaddyJson.abi, PADDY_ADDRESS)
		this.buyPaddy = this.buyPaddy.bind(this);
		window.ethereum.on('accountsChanged', function (accounts) {
			this.setState({ account: accounts[0] })
		});

		this.state = {
			account: '',
		}
	}

	async componentDidMount() {
		let account = await window.ethereum.selectedAddress;
		this.setState({ account });

	}

	async buyPaddy() {
		try {
			const contractData = this.contract.methods.buy().encodeABI();
			console.log("contractData", contractData)
			const amount = this.web3.utils.toWei("0.000005", "ether");
			const gasPrice = await this.web3.eth.getGasPrice();

			const transactionParameters = {
				gasPrice: this.web3.utils.numberToHex(gasPrice), // customizable by user during MetaMask confirmation.
				to: PADDY_ADDRESS, // Required except during contract publications.
				from: this.state.account, // must match user's active address.
				value: amount, // Only required to send ether to the recipient from the initiating external account.
				data: contractData, // Optional, but used for defining smart contract creation and interaction.
			};

			// txHash is a hex string
			// As with any RPC call, it may throw an error
			const txHash = await window.ethereum.request({
				method: 'eth_sendTransaction',
				params: [transactionParameters],
			});
			if(!txHash){
				console.log("something went wrong! retry later.")
			}
		} catch (error) {
			alert("there was an error with the transaction. try again later.");
			console.error(error)
		}
	}

	render() {
		const account = 
			this.state.account !== null ? 
				`your account ends with: ...${this.state.account.substring(this.state.account.length - 6, this.state.account.length)}` : 
				`refresh the page to connect metamask!`
		return (
			<Card className={styles.BuyTokenCard}>
				<Card.Body>
					<Card.Title></Card.Title>
					<Card.Text>{account}</Card.Text>
					<Card.Text>PaddyCoin address: 0xAb5e9c857b5747D38138CC4Ac2788aA87Bd5FfD4 - use it to add the token to metamask wallet</Card.Text>
					<Card.Header >Price: 0.005 ETH</Card.Header>
					<Button id={styles.BuyButton} onClick={this.buyPaddy}>Buy Now!</Button>
				</Card.Body>
			</Card>

		);
	}
}


export default BuyTokenCard;